LOCAL CRLF
CRLF = CHR(13) + CHR(10)
Response.Write([<!DOCTYPE html>]+ CRLF +;
   [<html>]+ CRLF +;
   [<head>]+ CRLF +;
   [<style>]+ CRLF +;
   [html, body]+ CRLF +;
   [{]+ CRLF +;
   [	font-family: Tahoma, Arial, sans-serif;]+ CRLF +;
   [	font-size: 14.25px;]+ CRLF +;
   [	overflow: auto;]+ CRLF +;
   [	margin: 0;] + CRLF )
Response.Write([}]+ CRLF +;
   [body {]+ CRLF +;
   [  margin: 5px 15px 10px;]+ CRLF +;
   [}  ]+ CRLF +;
   [h1, h2, h3]+ CRLF +;
   [{]+ CRLF +;
   [	color: steelblue; ]+ CRLF +;
   [	margin: 0.6em 0 0.4em 0;]+ CRLF +;
   [	padding: 0;]+ CRLF +;
   [}] + CRLF )
Response.Write([h4, h5 ]+ CRLF +;
   [{]+ CRLF +;
   [	color: #444;]+ CRLF +;
   [	margin: 0.2em 0 0.1em 0; ]+ CRLF +;
   [	padding: 0;]+ CRLF +;
   [}]+ CRLF +;
   [h1 {]+ CRLF +;
   [	padding-bottom: 8px;]+ CRLF +;
   [	border-bottom: 1px solid #ddd;]+ CRLF +;
   [	font-size: 1.75em;] + CRLF )
Response.Write([}]+ CRLF +;
   [hr {	 ]+ CRLF +;
   [	 height: 0;]+ CRLF +;
   [	 border: 0;]+ CRLF +;
   [	 border-top: 1px solid #eee;]+ CRLF +;
   [	 margin: 20px 0;]+ CRLF +;
   [}]+ CRLF +;
   [blockquote {]+ CRLF +;
   [	 border-left: 7px solid #999;]+ CRLF +;
   [	 padding: 5px 5px 5px 15px;] + CRLF )
Response.Write([	 margin-left: 15px;]+ CRLF +;
   [}]+ CRLF +;
   [</style>]+ CRLF +;
   [</style>]+ CRLF +;
   [</head>]+ CRLF +;
   [<h1>Script Page Example</h1>]+ CRLF +;
   [<p>The following content was generated from a script.</p>]+ CRLF +;
   [<h3>FoxPro Version</h3>]+ CRLF +;
   [<p>])

Response.Write(TRANSFORM( EVALUATE([ VERSION() ]) ))

Response.Write([</p>]+ CRLF +;
   [<h3>Go the time?</h3>]+ CRLF +;
   [<p>])

Response.Write(TRANSFORM( EVALUATE([ Time() ]) ))

Response.Write([</p>]+ CRLF +;
   [<h3>Maybe not so witty</h3>]+ CRLF +;
   []+ CRLF +;
   [<div style="background: #f5f5f5; padding: 10px;font-size: 1.2em;border-radius: 6px">]+ CRLF +;
   [	 <blockquote>]+ CRLF +;
   [])

Response.Write(EncodeHtml(TRANSFORM( EVALUATE([ pcMessage ]) )))

Response.Write([]+ CRLF +;
   [	 <div style="font-size: 0.825em; margin-left: 20px;margin-top: 10px; font-style: italic">]+ CRLF +;
   [		  -- ])

Response.Write(EncodeHtml(TRANSFORM( EVALUATE([ SUBSTR(SYS(0), AT("#",SYS(0)) + 2) ]) )))

Response.Write([]+ CRLF +;
   [	 </div>]+ CRLF +;
   [	 </blockquote>]+ CRLF +;
   [</div>]+ CRLF +;
   []+ CRLF +;
   [<hr/>]+ CRLF +;
   []+ CRLF +;
   [<h4>Do you want to continue?</h4>]+ CRLF +;
   [</body>]+ CRLF +;
   [</html>])
