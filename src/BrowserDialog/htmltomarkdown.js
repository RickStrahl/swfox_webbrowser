function htmltomarkdown(html) {
    try {
        return toMarkdown(html, {gfm: true});
    } catch (ex) {
        return null;
    }
}


var page = {
    fox: null,
    contactname: "Bill Blowback",
    // object map methods and properties have to be ALL LOWER CASE!
    sayhello: function(name) {
        return "Hello from swfox, " + name;
    },
    // object map methods and properties have to be ALL LOWER CASE!
    passobject: function(pos) {
        alert("map: " + pos.column + " " + pos.row);
    }
}

function passObject(pos) {
    alert(pos.column + " " + pos.row);
    page.fox = pos;
    return page;
}
function initializeInterop(fox){
    page.fox = fox;
    return page;
}
