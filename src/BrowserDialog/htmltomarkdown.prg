LPARAMETERS lcMarkdown 

IF EMPTY(lcMarkdown)
   lcMarkdown = "<h3>What's up <b>doc</b></h3>"
ENDIF   

DO wwUtils
DO wwAPI
SET CLASSLIB TO code\wwDialogs additive

PUBLIC goMarkdownDialog
IF TYPE("goMarkdownDialog") # "O"
	goMarkdownDialog = CREATEOBJECT("wwBrowserDialog")
	goMarkdownDialog.Navigate(FULLPATH(".\browserdialog\htmltomarkdown.htm"))
	IF !goMarkdownDialog.WaitForReadyState()   	 
	   RETURN ""
	ENDIF
ENDIF

DOEVENTS

*** Call the htmlToMarkdown function inside of the browser
lcResult = goMarkdownDialog.oBrowser.Document.parentWindow.htmlToMarkdown(lcMarkdown)
RETURN lcResult