LPARAMETERS lcURl

SET PROCEDURE TO BROWSERDOCUMENT ADDITIVE

IF EMPTY(lcUrl)
   lcUrl = "http://www.west-wind.com/"
   lcUrl = "http://localhost/"
ENDIF
    
PUBLIC oIe as InternetExplorer.Application
PUBLIC oDoc as MSHTML.HTMLDocument 
PUBLIC oEv

oIE = CREATEOBJECT("InternetExplorer.Application")
oIE.Visible = .T.

oIE.Navigate(lcUrl)

*** Now wait for the document to continue
*** loading (function below)
IF !WaitForReadyState(oIE,4,10000)
   RETURN .Null.
ENDIF

oDoc = oIE.Document

RETURN oDoc



FUNCTION WaitForReadyState
LPARAMETERS loIE, lnReadyState, lnMilliSeconds
LOCAL lnX

*** Allow for download to start
DOEVENTS

IF EMPTY(lnReadyState)
  lnReadyState = 4
ENDIF

IF EMPTY(lnMilliSeconds)
   lnMilliSeconds = 5000
ENDIF

DOEVENTS

DECLARE INTEGER Sleep IN WIN32API INTEGER nMSecs
lnX = 0
DO WHILE loIE.ReadyState # lnReadyState AND ;
         lnX * 5 < lnMilliSeconds
  DOEVENTS
  
  lnX = lnX + 1 
  Sleep(5)

  *** Check if IE was closed
  IF TYPE("loIE.ReadyState") # "N"
     EXIT
  ENDIF
  *? loIe.ReadyState
ENDDO

IF lnX * 5 < lnMilliSeconds
  RETURN .T. && Not timed out
ENDIF

RETURN .F.
