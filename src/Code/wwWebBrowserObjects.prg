SET PROCEDURE TO MSHtmlObjects ADDITIVE
SET PROCEDURE TO wwWebBrowserObjects ADDITIVE

*** Event Forwarding class
DEFINE CLASS wwWebBrowserDocumentEvents as HtmlDocumentEvents
   
   *** MUST CREATE A __BROWSERCONTROL PUBLIC variable
   *** visible to this routine.
   *** Can't use reference since this would lock the control
   *** for unloading.
   
   oBrowser = null
     
   PROCEDURE HTMLDocumentEvents_oncontextmenu() AS LOGICAL
   *** Forward the call to the Form Handler
   RETURN this.oBrowser.OnContextMenu()
   ENDPROC

ENDDEFINE
