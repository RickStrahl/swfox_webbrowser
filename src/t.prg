
DO FORM WebBrowser NAME Browser

Browser.oBrowser.Navigate("https://west-wind.com")

ACTIVATE SCREEN
Browser.WaitForReadyState()

loDoc = Browser.oBrowser.Document

loNodes = loDoc.querySelectorAll(".tab-content a")

ACTIVATE SCREEN
? "Link count: " + TRANSFORM(loNodes.length)

FOR lnX = 0 TO loNodes.Length -1
   loNode = loNodes.item(lnX)
   ? loNode.href
ENDFOR
