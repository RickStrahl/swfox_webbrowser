# Sample code for Southwest Fox 2017 Web Browser Control Session

This repository contains the source code for the Web Browser Control session that demonstrates how to use the Web Browser control in a number of different scenarios to enhance FoxPro applications.

* [White Paper](https://bitbucket.org/RickStrahl/swfox_webbrowser/raw/master/Documents/Strahl_WebBrowser.pdf)
* [Slides](https://bitbucket.org/RickStrahl/swfox_webbrowser/raw/master/Documents/Strahl_WebBrowser.pptx)
* [West Wind Html Help Builder](https://helpbuilder.west-wind.com)


### Running FoxPro Samples
The `src` folder contains all the samples shown in this session. To run the samples make sure you set up the environment.

* Start Visual FoxPro
* cd <Repository Localation>
* `DO START`

This starts the Startup Menu form from which you can launch other samples. `Start.prg` also sets paths to all the samples so you can launch and edit them individually. Each of the samples is separated into their own folder.

